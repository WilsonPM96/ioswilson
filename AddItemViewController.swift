//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Wilson Gabriel Ramos Bravo on 9/5/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {
    
    @IBOutlet weak var tittleTextField: UITextField!
    
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    var itemManager: ItemManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTittle = tittleTextField.text ?? ""  //con ?? se rompe la opcionalidad
        
        let itemLocation = locationTextField.text ?? ""
        
        let itemDescription = descriptionTextField.text ?? ""
        
        let item = Item(tittle: itemTittle, location: itemLocation , description: itemDescription)
        
        if item.tittle == "" {
            showAlert(tittle: "Error", message: "Title is required")
            
        }
        else{
            itemManager?.toDoItems += [item]
            
            let alert = UIAlertController(title: "Item Saved", message: "OK", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Accept", style: .default, handler: { (alertAction) in
                 self.navigationController?.popViewController(animated: true)
            })
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
           
        }
        
        
    }
    func showAlert(tittle:String, message:String){
        let alert = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    
}
