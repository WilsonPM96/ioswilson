//
//  ViewController.swift
//  ToDo
//
//  Created by Wilson Gabriel Ramos Bravo on 8/5/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let itemManager = ItemManager()

    @IBOutlet weak var itemsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddItemSegue"{
        let destination = segue.destination as! AddItemViewController
        destination.itemManager = itemManager
        }
        if segue.identifier == "toInfoViewSegue" {
            let destination = segue.destination as! RemoteViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)
        }
    }
    
    //MARK: Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItems.count
        }
        return itemManager.doneItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell") as! ItemTableViewCell
        
        if indexPath.section == 0 {
            cell.titleLabel.text = itemManager.toDoItems[indexPath.row].tittle
            cell.locationLabel.text = itemManager.toDoItems[indexPath.row].location
        } else {
            cell.titleLabel.text = itemManager.doneItems[indexPath.row].tittle
            cell.locationLabel.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "toInfoViewSegue", sender: self)
        }
        
        
        
    }   //se acciona cuando el usuario selecciona una celda de la tabla
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To Do" : "Done"
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return indexPath.section==0 ? "Check" : "UnCheck"
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
                itemManager.checkItem(index: indexPath.row)
        } else {
            itemManager.unCheckItem(index: indexPath.row)
        }
        
        itemsTableView.reloadData()
    }


}

