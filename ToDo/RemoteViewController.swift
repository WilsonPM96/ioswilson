//
//  RemoteViewController.swift
//  
//
//  Created by Wilson Gabriel Ramos Bravo on 16/5/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class RemoteViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    var itemInfo:(itemManager:ItemManager,index: Int)?  //hace referencia al item y a la celda que ocupa
    //var item:Item?  //copia del item
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].tittle
        locationLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].location
        descriptionLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].description
    }
    
    
    @IBAction func checkButtonPressed(_ sender: Any) {
        itemInfo?.itemManager.checkItem(index: (itemInfo?.index)!)
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
    


}
