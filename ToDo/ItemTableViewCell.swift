//
//  ItemTableViewCell.swift
//  ToDo
//
//  Created by Wilson Gabriel Ramos Bravo on 22/5/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
