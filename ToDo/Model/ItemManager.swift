//
//  ItemManager.swift
//  ToDo
//
//  Created by Wilson Gabriel Ramos Bravo on 9/5/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import Foundation

class ItemManager {
    var toDoItems:[Item] = []
    var doneItems:[Item] = []
    
    func checkItem(index:Int){
        
    let item = toDoItems.remove(at: index)
    doneItems += [item]
    }
    
    func unCheckItem(index: Int){
        let item = doneItems.remove(at: index)
        toDoItems += [item]
    }
}
